=== Version 0.22, 2006-09-23 ===========================================

 - improved support for character encodings; see variable
   *PG-CLIENT-ENCODING* (UTF8 encoding tested with SBCL and CLISP
   with PostgreSQL 8.1).

 - fixes to the support for prepared statements (or "execution plans";
   see the README for details of the API) on the v3 frontend/backend
   protocol.

 - on CL implementations that support Unix sockets, the HOST argument
   to PG-CONNECT may designate the directory containing the local
   PostgreSQL unix socket (often "/var/run/postgresql/"). The HOST
   argument is assumed to designate a local directory rather than a
   hostname when its first character is #\/. You may need to modify
   authentication options in the PostgreSQL configuration file
   pg_hba.conf to allow connections over a unix-domain socket where
   the databse username is not equal to your ident tokens. This is an
   incompatible change to previous support for unix-domain sockets
   with CMUCL (previously a HOST of NIL told pg-dot-lisp to connect
   to a unix-domain socket whose name was hardwired into the library).
   This support currently exists for SBCL, CMUCL and OpenMCL. 

 - many other bugfixes


=== Version 0.21, 2003-05-05 ===========================================

 - added support for the v3 frontend/backend protocol, used by
   PostgreSQL version 7.4 and up (thanks for Peter Van Eynde).
   pg-dot-lisp will attempt to connect to your database server using
   the new protocol, and upon failure will reconnect using the older
   protocol. To avoid this once-per-connection overhead if you know
   you're only using older PostgreSQL versions, use PG-CONNECT/V2
   instead of PG-CONNECT. 
 
 - split out functionality into more files

 - added preliminary support for character encodings, for when the
   encoding used by PostgreSQL for TEXT data differs from that used
   by the Common Lisp implementation for strings.


=== Version 0.20 (unreleased) ==========================================

 - added more tests for BOOLEAN types, to check the handling of
   PostgreSQL errors (violation of an integrity constraint leads to an
   error of type PG:BACKEND-ERROR being signaled). For CMUCL users who
   use the multiprocessing support, there's a test that runs several data
   producers and a consumer in different threads. 

 - error condition names (POSTGRESQL-ERROR, AUTHENTICATION-FAILURE,
   BACKEND-ERROR are now exported from the PG package. 



=== Version 0.19, 2003-10-10 ===========================================

 - new multi-file organization: split out the system-dependent parts
   and the tests into separate files. An ASDF system description is
   provided in the file pg.asd. 

 - change to WITH-PG-TRANSACTION: the previous version would abort the
   current transaction upon encountering an error, which made
   debugging difficult. The new version (thanks to Daniel Barlow)
   maintains the transaction open until you have exited the debugger.

 - support for connecting to PostgreSQL using a local socket instead
   of using TCP/IP, when you're on the same host as the backend. To
   enable, use a NIL host argument to PG-CONNECT. This makes it
   possible to connect to the database without enabling TCP/IP
   connections in the backend ("-i" option), and (depending on the
   access configuration parameters specified in pg_hba.conf) possibly
   without supplying a password. This is currently only supported for
   CMUCL and SBCL.
   
 - parser support for the INTERVAL type (you get this by subtracting
   two timestamps). They are coerced by pg-dot-lisp to a number of
   seconds, represented in floating point.

 - new configuration variable *PG-CLIENT-ENCODING*, that supports
   client-side encoding of text data, as per
   <http://www.postgresql.org/docs/7.3/static/multibyte.html>.
   Function PG-CLIENT-ENCODING, and corresponding setf
   function, that allows you to retrieve and modify the current client
   encoding.

 - new configuration variable *PG-DATE-STYLE* that allows you to
   change the style in which date types are printed. Function
   PG-DATE-STYLE that allows you to retrieve (and modify via its SETF
   function) the current backend's date style.

 - CMUCL: loading the file cmucl-install-subsystem.lisp (as a user who
   has write access to the directory where CMUCL is installed) will
   cause this package to be installed as a CMUCL "subsystem", that can
   thereafter be loaded by saying "(require :pg)".

 - SBCL: fix for new sb-bsd-sockets

 - support for a new Common Lisp implementation, Armed Bear Lisp for
   the JVM




=== Version 0.18, 2003-06-01 ============================================

 - Fix for parsing of TIMESTAMP fields in PostgreSQL 7.3, thanks to
  James Anderson. These can now contain timezone and millisecond
  fields.

